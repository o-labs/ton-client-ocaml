let () =
  let c = ref "req" in
  Arg.parse [] (fun s -> c:= s) "test_unix.exe <action = 'req' or 'ws'>";
  if !c = "req" then EzLwtSys.run Test_lib.req
  else if !c = "ws" then EzLwtSys.run Test_lib.ws
