let () =
  Js_of_ocaml.Js.export "test" @@ object%js
    method req = Test_lib.req ()
    method ws = Test_lib.ws ()
  end
