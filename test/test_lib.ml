open Ton
open Lwt.Infix
open Request

let req () =
  let output = Encoding.transactions_enc in
  let service = service output in
  query ~base:dev_base ~msg:"transactions" service (transactions ~limit:10 []) >|= function
  | Error e ->
    Format.printf "%s@." (EzRequest_lwt.string_of_error (fun exn -> Some (Printexc.to_string exn)) e)
  | Ok l ->
    Format.printf "%s@."(EzEncoding.construct ~compact:false output l)

let ws () =
  let open EzWs in
  let output = Json_encoding.(obj1 (req "blocks" Encoding.block_enc)) in
  let service = ws_service output in
  let react _action = function
    | Error e -> Lwt.return_ok @@ Format.printf "Error: %s@." (Printexc.to_string e)
    | Ok payload ->
      Lwt.return_ok @@ Format.printf "Ok %s@." (EzEncoding.construct ~compact:false output payload) in
  subscribe ~base:dev_base ~msg:"ws_blocks" ~react service (Request.blocks []) >>= function
  | Error e ->
    Lwt.return @@ Format.printf "Error: %s@." e
  | Ok {conn; _} -> conn >|= function
    | Error e -> Format.printf "Connection stopped with error: %s@." e
    | Ok () -> Format.printf "Connection stopped normally@."
