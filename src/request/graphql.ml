type arg = [
  | `string of string
  | `int of int
  | `float of float
  | `bool of bool
  | `raw of string
  | `var of string
  | `obj of (string * arg) list ]

type root_query = {
  content : query list;
  kind : string option;
  name : string option
}

and query =
  | Scalar of {
      name : string;
      args : (string * arg) list }
  | Field of {
      name : string;
      args : (string * arg) list;
      fields : query list;
      alias : string option }
  | Root of root_query

let rec string_of_arg ?(compact=true) (o : arg) = match o with
  | `string s -> Format.sprintf "%S" s
  | `raw s -> s
  | `int i -> string_of_int i
  | `float f -> string_of_float f
  | `bool b -> string_of_bool b
  | `var v -> "$" ^ v
  | `obj l ->
     let sep = if compact then " " else "\n" in
     Format.sprintf "{%s%s%s}"
       sep
       (String.concat sep @@ List.map (fun (s, o) -> Format.sprintf "%s: %s" s (string_of_arg o)) l)
       sep

let string_of_args ?(compact=true) args =
  let sep = if compact then " " else "\n" in
  match args with
  | [] -> ""
  | _ ->
    Format.sprintf "(%s)" @@
    String.concat sep @@
    List.map (fun (s, o) -> Format.sprintf "%s: %s" s (string_of_arg o)) args

let rec string_of_query ?(compact=true) q =
  let sep = if compact then " " else "\n" in
  match q with
  | Scalar { name; args } -> name ^ string_of_args ~compact args
  | Field { name; args; fields; alias } ->
    let alias = match alias with None -> "" | Some s -> s ^ ": " in
    Format.sprintf "%s%s%s {%s%s%s}"
      alias name (string_of_args ~compact args)
      sep (String.concat sep (List.map (string_of_query ~compact) fields)) sep
  | Root {content; kind; name} ->
    let content =
      Format.sprintf "{%s%s%s}"
        sep (String.concat sep (List.map (string_of_query ~compact) content)) sep in
    match kind with
    | None -> content
    | Some kind -> match name with
      | None -> kind ^ " " ^ content
      | Some name -> kind ^ " " ^ name ^ " " ^ content

let root_query_enc = Json_encoding.(
    conv
      (fun q -> string_of_query (Root q))
      (fun _ -> failwith "destruct of query not handled") string)

type request = {
  query : root_query;
  operation_name : string option; [@opt] [@key "operationName"]
  variables : (string * Json_repr.ezjsonm) list; [@assoc] [@dft []]
}
[@@deriving json_encoding]

let scalar ?(args=[]) name = Scalar {name; args}
let field ?(args=[]) ?alias name fields = Field {name; args; fields; alias}

let request ?kind ?name ?(variables=[]) field = {
  query = {content = [ field ]; kind; name};
  operation_name = name;
  variables }

let astring s : [< arg] = `string s
let araw s : [< arg] = `raw s
let avar s : [< arg] = `var s
let aint i : [< arg] = `int i
let afloat f : [< arg] = `float f
let abool b : [< arg] = `bool b
let aobj l : [< arg] = `obj l
