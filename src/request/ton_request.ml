open EzAPI
open Graphql

let aeq name value = [ name, aobj [ "eq", value ] ]
let alimit n = [ "limit", aint n ]
let aorder ?(direction="DESC") name =
  [ "orderBy", aobj [ "path", `string name; "direction", `raw direction ] ]
let afilter l = [ "filter", aobj l ]
let alist ?limit ?order ?filter args =
  let args = match filter with None -> args | Some l -> afilter l @ args in
  let args = match order with None -> args | Some (name, direction) -> aorder ?direction name @ args in
  match limit with None -> args | Some n -> alimit n @ args

let version = field "info" [ scalar "version"; scalar "time" ]

let account_info = [
  scalar "id";
  scalar "acc_type";
  scalar ~args:["format", araw "DEC"] "balance";
]

let accounts ?limit ?order ?filter args =
  let args = alist ?limit ?order ?filter args in
  field ~args "accounts" account_info

let account id =
  accounts ~filter:(aeq "id" (astring id)) []

let ext_blk_ref = [
  scalar ~args:["format", araw "DEC"] "end_lt";
  scalar "seq_no";
  scalar "root_hash";
  scalar "file_hash" ]

let block_value_flow = [
  scalar ~args:["format", araw "DEC"] "to_next_blk";
  scalar ~args:["format", araw "DEC"] "fees_collected";
  scalar ~args:["format", araw "DEC"] "minted";
]

let block_info = [
  scalar "id";
  scalar "status";
  scalar "workchain_id";
  scalar "shard";
  scalar "seq_no";
  field "prev_ref" ext_blk_ref;
  scalar "gen_utime";
  scalar "created_by";
  scalar "tr_count";
  scalar "key_block";
  field "value_flow" block_value_flow;
]

let blocks ?limit ?order ?filter args =
  let args = alist ?limit ?order ?filter args in
  field ~args "blocks" block_info

let block (id : [< `int of int | `string of string ]) =
  let filter = match id with `string s -> aeq "id" (astring s) | `int i -> aeq "seq_no" (aint i) in
  blocks ~filter []

let head () = blocks ~limit:1 ~order:("seq_no", None) []

let message_info = [
  scalar "id";
  scalar "msg_type";
  scalar "status";
  scalar "block_id";
  scalar "src";
  scalar "dst";
  scalar ~args:["format", araw "DEC"] "value"
]

let messages ?limit ?order ?filter args =
  let args = alist ?limit ?order ?filter args in
  field ~args "messages" message_info

let transaction_info = [
  scalar "id";
  scalar "tr_type";
  scalar "status";
  scalar "block_id";
  scalar "account_addr";
  scalar ~args:["format", araw "DEC"] "total_fees";
  scalar ~args:["format", araw "DEC"] "balance_delta";
  field "in_message" message_info;
]

let transactions ?limit ?order ?filter args =
  let args = alist ?limit ?order ?filter args in
  field ~args "transactions" transaction_info

let transaction id =
  transactions ~filter:(aeq "id" (astring id)) []

let block_transactions id =
  transactions ~filter:(aeq "block_id" (astring id)) []

let account_transactions id =
  transactions ~filter:(aeq "account_addr" (astring id)) []

(** requests *)

let dev_base = TYPES.BASE "https://net.ton.dev"
let base = TYPES.BASE "https://main.ton.dev"

let service
  ?section ?name ?descr ?params ?security ?register ?input_example ?output_example ?errors
  (output: 'ourput Json_encoding.encoding) :
  (request, 'output, 'error, 'security) post_service0 =
  post_service
    ?section ?name ?descr ?params ?security ?register ?input_example ?output_example ?errors
    ~input:request_enc
    ~output:Json_encoding.(obj1 (req "data" output))
    Path.(root // "graphql")

let query ?(base=base) ?msg ?variables service query =
  let input = Graphql.request ?variables query in
  EzReq_lwt.post0 ?msg ~input base service

type 'a ws_content =
  | WsInit of { payload : unit [@encoding Json_encoding.empty] } [@kind "connection_init"] [@kind_label "type"]
  | WsStart of { id : string; payload : request } [@kind] [@kind_label "type"]
  | WsData of { id: string; payload : 'a [@obj1 "data"] } [@kind] [@kind_label "type"]
  | WsOther of (string [@obj1 "type"])
[@@deriving json_encoding]

let ws_service
    ?section ?name ?descr ?params ?security ?register ?input_example ?output_example ?errors
    (output: 'ourput Json_encoding.encoding) :
    ('output ws_content, 'output ws_content, 'error, 'security) ws_service0 =
  raw_service
    ?section ?name ?descr ?params ?security ?register ?input_example ?output_example ?errors
    ~input:(Json (ws_content_enc output))
    ~output:(Json (ws_content_enc output))
    Path.(root // "graphql")

let subscribe ?(base=base) ?msg ?error ?variables ~react service query =
  let open EzWs in
  let react action = function
    | Error e -> react action (Error e)
    | Ok (WsData {payload; _}) -> react action (Ok payload)
    | Ok _ -> Lwt.return_ok () in
  Lwt.bind
    (EzWs.connect0 ?msg ?error ~protocols:["graphql-ws"] ~react base service)
    (function
      | Error e -> Lwt.return_error e
      | Ok ws ->
        Lwt.bind
          (ws.action.send @@ WsInit { payload = () })
          (function
            | Error e -> Lwt.map (fun _ -> Error e) (ws.action.close None)
            | Ok () ->
              let payload = Graphql.request ~kind:"subscription" ?variables query in
              Lwt.bind
                (ws.action.send @@ WsStart { id = "1"; payload })
                (function
                  | Error e -> Lwt.map (fun _ -> Error e) (ws.action.close None)
                  | Ok () -> Lwt.return_ok ws)))
