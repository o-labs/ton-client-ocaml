all: build

build:
	dune build src

dev: build
	dune build --profile release test

clean:
	dune clean
